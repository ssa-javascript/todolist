
function updateItemStatus(){
  var chId = this.id.replace('cb_', '');
  var itemText = document.getElementById('item_' + chId);
 
  if(this.checked){
    itemText.className = 'checked';
  } else {
    itemText.className = '';
  }
}
 
function renameItem(){
  //this === span
  var newText = prompt("what should this item be renamed to?");
  if(!newText || newText === "" || newText === " ") return false; // blank 방지
  var spanId = this.id.replace('pencilIcon_', '');
  var span = document.getElementById('item_' + spanId);
 
  span.innerText = newText;

  var listItem = document.getElementById('li_' + spanId);
  var listItemParentId = listItem.parentElement;
  if(listItemParentId == donelist){
    updateDBItem(spanId,newText,0);
  } else {
    updateDBItem(spanId,newText,1);
  }
}
 
var donelist = document.getElementById('donelist');
function moveItem(){
  //this === span
  var listItemId = this.id.replace('li_', '');
  var listItem = document.getElementById('li_' + listItemId);
  var listItemParentId = listItem.parentElement;
  //console.log(listItemParentId);
  if(listItemParentId == donelist){
    todolist.appendChild(listItem);
    updateDBItem(listItemId,document.getElementById('item_'+listItemId).innerText,0);
  } else {
    donelist.appendChild(listItem);
    updateDBItem(listItemId,document.getElementById('item_'+listItemId).innerText,1);
  }
}
 
function deleteItem(donelist){
  //this === minusIcon
  var listItemId = this.id.replace('minusIcon_', '');
  document.getElementById('li_' + listItemId).style.display = "none";
  deleteDBItme(listItemId);
}
 
function mouseover(){
  //this === li
  var pencilIconId = this.id.replace('li_','');
  var pencilIcon = document.getElementById('pencilIcon_' + pencilIconId);
  var minusIcon = document.getElementById('minusIcon_' + pencilIconId);
 
  pencilIcon.style.visibility = 'visible';
  minusIcon.style.visibility = 'visible';
}
 
function mouseout(){
  //this === li
  var pencilIconId = this.id.replace('li_','');
  var pencilIcon = document.getElementById('pencilIcon_' + pencilIconId);
  var minusIcon = document.getElementById('minusIcon_' + pencilIconId);
 
  pencilIcon.style.visibility = 'hidden';
  minusIcon.style.visibility = 'hidden';
}

function settingItem(data){
  if(data.done == '0'){
    addNewItem(document.getElementById('todolist'),data.time_id, data.text);
  } else {
    addNewItem(document.getElementById('donelist'),data.time_id, data.text);
  }
}


function addNewItem(list,id, itemText){
 
 
  var listItem = document.createElement('li');
  listItem.id = 'li_' + id;
  listItem.ondblclick = moveItem;
  listItem.addEventListener('mouseover', mouseover);
  listItem.addEventListener('mouseout', mouseout);
 
  var span = document.createElement('span');
  span.id = 'item_' + id;
  span.innerText = itemText;
 
  var pencilIcon = document.createElement('i');
  pencilIcon.className = 'fas fa-pencil-alt';
  pencilIcon.id = 'pencilIcon_' + id;
  pencilIcon.onclick = renameItem;
 
  var minusIcon = document.createElement('i');
  minusIcon.className = 'fas fa-minus-square';
  minusIcon.id = 'minusIcon_' + id;
  minusIcon.onclick = deleteItem;
 
  listItem.appendChild(span);
  listItem.appendChild(minusIcon);
  listItem.appendChild(pencilIcon);
  list.appendChild(listItem);

}
 
var inputText = document.getElementById('inputText');
inputText.focus();
 
inputText.onkeyup = function(event){
  //Event.which (13) === ENTER Key!!
  if(event.which === 13){
    var itemText = inputText.value;
    if(itemText === "" || itemText === " ") return false; // blank 방지
    var date = new Date();
    var id = "" + date.getHours() + date.getMinutes() + date.getSeconds() + date.getMilliseconds();
   
    addNewItem(document.getElementById('todolist'),id, itemText);
    insertDBItem(id,itemText);
    inputText.focus();
    inputText.select();
  }
};

function loadDBItem(){
  $.ajax({
    url: 'http://localhost:3000/getList',
    type: 'GET'
}).then((data, textStatus, jqXHR) => {
    console.log(data);
    data.forEach(element => {
        settingItem(element);
    });
})
}

function insertDBItem(id,text){
  $.ajax({
    url: 'http://localhost:3000/insetItem',
    type: 'GET',
    dataType: 'json',
    data:{id:id,text:text}
}).then((data, textStatus, jqXHR) => {
    console.log(data);
})
};
function updateDBItem(id,text,done){
  $.ajax({
    url: 'http://localhost:3000/updateItem',
    type: 'GET',
    dataType: 'json',
    data:{id:id,text:text,done:done}
}).then((data, textStatus, jqXHR) => {
    console.log(data);
})
}
function deleteDBItme(id){
  $.ajax({
    url: 'http://localhost:3000/deleteItem',
    type: 'GET',
    dataType: 'json',
    data:{id:id}
}).then((data, textStatus, jqXHR) => {
    console.log(data);
})
}

loadDBItem();